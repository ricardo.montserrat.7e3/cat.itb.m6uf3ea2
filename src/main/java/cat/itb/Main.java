package cat.itb;

import cat.itb.Database.MyMongoDatabase;

public class Main
{
    public static final String JSONS_FILES_PATH = "src/main/resources/Jsons/";
    public static final String VIRTUAL_MACHINE_IP = "192.168.56.101";

    public static final String DATABASE_NAME = "itb";

    public static void main(String[] args)
    {
        MyMongoDatabase mongo = new MyMongoDatabase(VIRTUAL_MACHINE_IP);

        //Load database data file
        mongo.loadArrayPeople(DATABASE_NAME, "people2", JSONS_FILES_PATH + "people.json");

        //Confirm if collection exist
        System.out.println("\n\nDatabase Created!!!");
        mongo.showCollections(DATABASE_NAME);

        //Delete of collection
        System.out.println("\n\nDestroyed values: " + mongo.dropCollection(DATABASE_NAME, "people2"));

        //Confirm if collection exist
        System.out.println("\n\nDatabase Deleted!!!");
        mongo.showCollections(DATABASE_NAME);

        //Update products between 600 and 1000
        System.out.println("\n\nUpdated products: " + mongo.updateDocumentsWhereBetween(DATABASE_NAME, "products", "price", 600, 1000, "stock", 0));

        //Delete products between 400 and 600
        System.out.println("\n\nDeleted products: " + mongo.deleteDocumentsWhereBetween(DATABASE_NAME, "products", "price", 400, 600));

        //Load the json array with the correct formatting of products
        mongo.loadArrayJson(DATABASE_NAME, "books", JSONS_FILES_PATH + "books.json");
    }
}
