package cat.itb.pojo;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Person
{

	@SerializedName("address")
	private String address;

	@SerializedName("gender")
	private String gender;

	@SerializedName("latitude")
	private double latitude;

	@SerializedName("about")
	private String about;

	@SerializedName("registered")
	private String registered;

	@SerializedName("isActive")
	private boolean isActive;

	@SerializedName("picture")
	private String picture;

	@SerializedName("friends")
	private List<Friends> friends;

	@SerializedName("tags")
	private List<String> tags;

	@SerializedName("balance")
	private String balance;

	@SerializedName("randomArrayItem")
	private String randomArrayItem;

	@SerializedName("phone")
	private String phone;

	@SerializedName("name")
	private String name;

	@SerializedName("age")
	private int age;

	@SerializedName("email")
	private String email;

	@SerializedName("longitude")
	private double longitude;

	@SerializedName("company")
	private String company;

	public void setAddress(String address){
		this.address = address;
	}

	public String getAddress(){
		return address;
	}

	public void setGender(String gender){
		this.gender = gender;
	}

	public String getGender(){
		return gender;
	}

	public void setLatitude(double latitude){
		this.latitude = latitude;
	}

	public double getLatitude(){
		return latitude;
	}

	public void setAbout(String about){
		this.about = about;
	}

	public String getAbout(){
		return about;
	}

	public void setRegistered(String registered){
		this.registered = registered;
	}

	public String getRegistered(){
		return registered;
	}

	public void setIsActive(boolean isActive){
		this.isActive = isActive;
	}

	public boolean isIsActive(){
		return isActive;
	}

	public void setPicture(String picture){
		this.picture = picture;
	}

	public String getPicture(){
		return picture;
	}

	public void setFriends(List<Friends> friends){
		this.friends = friends;
	}

	public List<Friends> getFriends(){
		return friends;
	}

	public void setTags(List<String> tags){
		this.tags = tags;
	}

	public List<String> getTags(){
		return tags;
	}

	public void setBalance(String balance){
		this.balance = balance;
	}

	public String getBalance(){
		return balance;
	}

	public void setRandomArrayItem(String randomArrayItem){
		this.randomArrayItem = randomArrayItem;
	}

	public String getRandomArrayItem(){
		return randomArrayItem;
	}

	public void setPhone(String phone){
		this.phone = phone;
	}

	public String getPhone(){
		return phone;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setAge(int age){
		this.age = age;
	}

	public int getAge(){
		return age;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setLongitude(double longitude){
		this.longitude = longitude;
	}

	public double getLongitude(){
		return longitude;
	}

	public void setCompany(String company){
		this.company = company;
	}

	public String getCompany(){
		return company;
	}
}