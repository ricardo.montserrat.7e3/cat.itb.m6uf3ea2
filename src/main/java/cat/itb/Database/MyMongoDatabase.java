package cat.itb.Database;

import cat.itb.pojo.Person;
import com.google.gson.Gson;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientOptions;
import com.mongodb.MongoClientSettings;
import com.mongodb.ServerAddress;
import com.mongodb.client.MongoCollection;
import org.bson.Document;
import org.bson.codecs.configuration.CodecRegistry;
import org.bson.codecs.pojo.PojoCodecProvider;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.Arrays;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static com.mongodb.client.model.Filters.gte;
import static com.mongodb.client.model.Filters.lte;
import static com.mongodb.client.model.Updates.combine;
import static com.mongodb.client.model.Updates.set;
import static org.bson.codecs.configuration.CodecRegistries.fromProviders;
import static org.bson.codecs.configuration.CodecRegistries.fromRegistries;

public class MyMongoDatabase
{
    public static final String MONGO_BASE_PORT = ":27017";

    private static MongoClient client;

    public MyMongoDatabase(String ip)
    {
        CodecRegistry pojoCodecRegistry = fromRegistries(MongoClientSettings.getDefaultCodecRegistry(), fromProviders(PojoCodecProvider.builder().automatic(true).build()));
        client = new MongoClient(new ServerAddress(ip + MONGO_BASE_PORT), MongoClientOptions.builder().codecRegistry(pojoCodecRegistry).build());
    }

    public void loadArrayJson(String database, String collection, String filePath)
    {
        String stringFile = "";
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) { stringFile = br.lines().collect(Collectors.joining()); }
        catch (Exception e) { System.out.println(e.toString()); }

        MongoCollection<Document> collectionMongo = client.getDatabase(database).getCollection(collection);
        collectionMongo.insertMany(Arrays.asList(new Gson().fromJson(stringFile, Document[].class)));
    }

    public void loadArrayPeople(String database, String collection, String filePath)
    {
        String stringFile = "";
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) { stringFile = br.lines().collect(Collectors.joining()); }
        catch (Exception e) { System.out.println(e.toString()); }

        MongoCollection<Person> collectionPeople = client.getDatabase(database).getCollection(collection, Person.class);
        collectionPeople.insertMany(Arrays.asList(new Gson().fromJson(stringFile, Person[].class)));
    }

    public void showCollections(String database) { client.getDatabase(database).listCollectionNames().forEach((Consumer<? super String>) System.out::println); }

    public long dropCollection(String database, String collection)
    {
        MongoCollection<Document> toDestroy = client.getDatabase(database).getCollection(collection);
        long collectionSize = toDestroy.countDocuments();
        toDestroy.drop();
        return collectionSize;
    }

    public long updateDocumentsWhereBetween(String database, String collection, String fieldToCompare, int minValue, int maxValue, String fieldToChange, int newValue)
    {
        return client.getDatabase(database).getCollection(collection).updateMany(combine(gte(fieldToCompare, minValue), lte(fieldToCompare, maxValue)), set(fieldToChange, newValue)).getModifiedCount();
    }

    public long deleteDocumentsWhereBetween(String database, String collection, String field, int minValue, int maxValue)
    {
        return client.getDatabase(database).getCollection(collection).deleteMany(combine(gte(field, minValue), lte(field, maxValue))).getDeletedCount();
    }
}
